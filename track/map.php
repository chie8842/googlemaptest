<?php

function getimagelist($lat1, $lon1, $lat2, $lon2) {
    $res = "";
    $dir = "/var/www/html/track/img/";
    chdir($dir);
    array_multisort(array_map('filemtime', ($files = glob("*.*"))), SORT_DESC, $files);
    foreach ($files as $filename) {
        $res = getexif($dir, $filename, $lat1, $lon1, $lat2, $lon2) . $res;
    }

    return $res;
}

function getimagelist0($lat1, $lon1, $lat2, $lon2) {
    // ディレクトリのパスを記述
    $dir = "/var/www/html/track/img/";
    if (is_dir($dir) && $handle = opendir($dir)) {
        // ループ処理
        while (($file = readdir($handle)) !== false) {
            if (filetype($path = $dir . $file) == "file") {
                getexif($dir, $file, $lat1, $lon1, $lat2, $lon2);
            }
        }
    }
}

function getexif0($dir, $filename, $lat1, $lon1, $lat2, $lon2) {
    $rate = "80";

    $exif = exif_read_data($dir . $filename);
    $lon = getGps($exif["GPSLongitude"], $exif['GPSLongitudeRef']);
    $lat = getGps($exif["GPSLatitude"], $exif['GPSLatitudeRef']);

    // check between lat and lon
    echo "var m_latlng1 = new google.maps.LatLng(" .
    $lat, "," . $lon . "); var marker1 = new google.maps.Marker({ position: m_latlng1, map: map" .
    " }); ";

    $res = '
  var myInfoWindow = new google.maps.InfoWindow({
  });
  myInfoWindow.open(map, marker1);
  google.maps.event.addListener(myInfoWindow, "closeclick", function() {
    google.maps.event.addListenerOnce(map, "click", function(event) {
      myInfoWindow.open(map, marker1);
    });
  });';

//         content: "<a href=/cgi-bin/detail.py?img=sample2.jpg&rate=77>

    echo $res;

    "// , img: " . $filename . " }); ";
}

function getexif($dir, $filename, $lat1, $lon1, $lat2, $lon2) {
    global $dbh, $lat0, $lon0;

    $exif = exif_read_data($dir . $filename);
    $lon = getGps($exif["GPSLongitude"], $exif['GPSLongitudeRef']);
    $lat = getGps($exif["GPSLatitude"], $exif['GPSLatitudeRef']);

    if ($lon == 0)
        return;

    if ($lat0 == 0) {
        $lat0 = $lat;
        $lon0 = $lon;
    }

    $res = "";
//    echo "getexif<br />".$dir . $filename;
    // 作成日
    $ctime = filectime($dir . $filename);

    // 心拍
    // SELECT FROM_UNIXTIME(TRUNCATE(UNIX_TIMESTAMP(bias) / 60, 0) * 60) AS time, avg(rate) FROM `raw`
//where rate > 0
//GROUP BY TRUNCATE(UNIX_TIMESTAMP(bias) / 60, 0)
//ORDER BY time  DESC
//limit 120

    $rate = "-";
    $q = "SELECT avg(rate) as avgrate FROM raw WHERE bias between from_unixtime(" 
            . (string)($ctime - 120)  . ") and from_unixtime(" . (string) ($ctime + 120) . ")";
    $stmt = $dbh->query($q);
    //     $stmt = $dbh->query($q);
//    echo $q;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $rate = round($row["avgrate"]);
//        echo $rate."<br />";
    }

// check between lat and lon
    $res .= "var m_latlng1 = new google.maps.LatLng(" .
            $lat . "," . $lon .
            ");";
    $res .= "var marker1 = new google.maps.Marker({ position: m_latlng1, map: map" . " }); ";

    $res .= '
  var myInfoWindow = new google.maps.InfoWindow({
  disableAutoPan: true,
    // 吹き出しに出す文
         content: "<a href=/detail.php?img=' . $filename . '&rate=' . $rate . '>画像とドキドキ</a>"
  });
  myInfoWindow.open(map, marker1);
  google.maps.event.addListener(myInfoWindow, "closeclick", function() {
    google.maps.event.addListenerOnce(map, "click", function(event) {
      myInfoWindow.open(map, marker1);
    });
  });';

//         content: "<a href=/cgi-bin/detail.py?img=sample2.jpg&rate=77>

    return $res;

    "// , img: " . $filename . " }); ";
}

function getGps($exifCoord, $hemi) {
    $degrees = count($exifCoord) > 0 ? gps2Num($exifCoord[0]) : 0;
    $minutes = count($exifCoord) > 1 ? gps2Num($exifCoord[1]) : 0;
    $seconds = count($exifCoord) > 2 ? gps2Num($exifCoord[2]) : 0;
    $flip = ($hemi == 'W' or $hemi == 'S') ? -1 : 1;
    return $flip * ($degrees + $minutes / 60 + $seconds / 3600);
}

function gps2Num($coordPart) {

    $parts = explode('/', $coordPart);

    if (count($parts) <= 0)
        return 0;

    if (count($parts) == 1)
        return $parts[0];

    return floatval($parts[0]) / floatval($parts[1]);
}

function gethrt() {
    global $dbh;

    $res = "";

    // 心拍

    $rate = "-";
    $q = "SELECT FROM_UNIXTIME(TRUNCATE(UNIX_TIMESTAMP(bias) / 60, 0) * 60) AS time, 
avg(rate) as avgrate, 
avg(lat) as avglat,
avg(lon) as avglon  FROM raw
where lat > 0 and rate > 0
GROUP BY TRUNCATE(UNIX_TIMESTAMP(bias) / 60, 0)
ORDER BY time  DESC
limit 60";
    $stmt = $dbh->query($q);
    //     $stmt = $dbh->query($q);
//    echo $q;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $rate = round($row["avgrate"]);
        $lat = $row["avglat"];
        $lon = $row["avglon"];

        $res .= "var m_latlng1 = new google.maps.LatLng(" .
                $lat . "," . $lon .
                ");";
        $res .= "var marker1 = new google.maps.Marker({ position: m_latlng1, map: map" . " }); ";

        $res .= '
  var myInfoWindow = new google.maps.InfoWindow({
  disableAutoPan: true,
    // 吹き出しに出す文
         content: "' . $rate . '"
  });
  myInfoWindow.open(map, marker1);
  google.maps.event.addListener(myInfoWindow, "closeclick", function() {
    google.maps.event.addListenerOnce(map, "click", function(event) {
      myInfoWindow.open(map, marker1);
    });
  });';
    }
    return $res;
}

// -------------------------------- main

$dsn = 'mysql:dbname=onigiri;host=localhost';
$user = 'root';
$password = 'onigiri';

try {
    $dbh = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
    print('Error:' . $e->getMessage());
    die();
}


$lat1 = $_GET["lat1"];
$lon1 = $_GET["lon1"];
$lat2 = $_GET["lat2"];
$lon2 = $_GET["lon2"];

// 中央値
// $lat0 = ($lat1+$lat2)/2;
// $lon0 = ($lon1+$lon2)/2;
$lat0 = 0; // getimagelistで設定
$lon0 = 0;

$res = getimagelist($lat1, $lon1, $lat2, $lon2);

$t1 = "function initialize() {";
$t2 = <<<EOD1
  var opts = {
    zoom: 13,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(document.getElementById("map_canvas"), opts);
EOD1;
$t3 = "  var latlng = new google.maps.LatLng(" . $lat0 . "," . $lon0 . ");";

echo $t1, $t3, $t2;
echo $res;
echo gethrt();
//echo $t3. "map.center = google.maps.LatLng(" . $lat0 . "," . $lon0 . ");map.setCenter(map.center);";
//echo "map.center = google.maps.LatLng(" . $lat0 . "," . $lon0 . ");map.setCenter(map.center);";
echo "}";

