<?php
$dsn = 'mysql:dbname=onigiri;host=localhost';
$user = 'root';
$password = 'onigiri';

try {
    $dbh = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
    print('Error:' . $e->getMessage());
    die();
}
?>
<html>
    <head>
        <title>Heart Rate Tracker</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        <meta http-equiv="refresh" content="3">
    </head>
    <body>

        <?php
        echo date("Y-m-d H:i:s");
        ?>
        <style type="text/css">
            .table3 {
                border-collapse: collapse;
            }
            .table3 th {
                background-color: #cccccc;
            }
        </style>
        <table class="table3" border=1>
            <tr><th>時刻</th>
                <th>kabayan</th>
                <th>lat</th>
                <th>lon</th>
            </tr>

            <?php
            $stmt = $dbh->query("SELECT bias,rate,lat,lon FROM raw WHERE bias order by bias desc limit 30");
       //     $stmt = $dbh->query($q);
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo "<tr><td>" . $row["bias"] . "</td>" .
                "<td>".$row["rate"] . "</td>".
                "<td>".$row["lat"] . "</td>".
                "<td>".$row["lon"] . "</td>".
                        "</tr>";
            }
            ?>
        </table>

    </body>
</html>
