#!/usr/bin/python
# -*- coding: utf-8 -*-
import cgi
import cgitb; cgitb.enable()
import os, sys, datetime

print "Content-Type: text/html; charset=utf-8"
print
UPLOAD_DIR = "../track/img/"
def save_uploaded_file (upload_dir):
    form = cgi.FieldStorage()
    if not form.has_key("file"):
        print "aファイルを入力してください"
        return
    fileitem = form["file"]
    if not fileitem.file:
        print "bファイルを入力してください"
        return
 
    if not fileitem.filename:
        print "cファイルを入力してください"
        return
#    if form["name"].value is "":
#        print "名前を入力してください"
#        return
    date = datetime.datetime.today().strftime("%Y%m%d_%H%M%S_")
    fout = file (os.path.join(upload_dir, date + os.path.basename(fileitem.filename)), 'wb')
    while 1:
        chunk = fileitem.file.read(100000)
        if not chunk: break
        fout.write (chunk)
    fout.close()
    print open("../upload_next.html", "r").read()


save_uploaded_file(UPLOAD_DIR)

