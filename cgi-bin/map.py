#!/usr/bin/python
import cgi
import cgitb
import MySQLdb

print "Content-type: text/html"
print
form = cgi.FieldStorage()

id = form.getvalue('id','0')
lat = form.getvalue('lat','0')
lon = form.getvalue('lon','0')
if lat == "0" and lon == "0":
  lat = 31.928463
  lon = 131.007751
print """
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8">
#    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

#    <title>circle hp template</title>
    <title>Google Maps JavaScript api</title>
    <!-- Bootstrap core CSS -->
    <link href="appli/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="appli/css/justified-nav.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="appli/javascripts/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="appli/javascripts/ie-emulation-modes-warning.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.13/angular.min.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript"
      src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="/track/map.php?id=aaa&lat=%f&lon=%f" type="text/javascript"></script>

    function clickEventFunc(event) {
      alert(event.latLng.toString());
    }

  </head>

  <body onload="initialize()">
   <form method="POST" action='map.py'>
      id <input type="text" name="id">
      lat <input type="text" name="lat">
      lon <input type="text" name="lon">
      <button type="submit" class="btn btn-default">Submit</button>
   </form>
   <div id="map_canvas" style="width:500px; height:300px"></div>

  </body>
</html>""" % (float(lat),float(lon))

print lat
print lon
