function initialize() {
  var latlng = new google.maps.LatLng(31.928463, 131.007751);
  var opts = {
    zoom: 14,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(document.getElementById("map_canvas"), opts);

  var m_latlng1 = new google.maps.LatLng(31.928463, 131.007751);
  var marker1 = new google.maps.Marker({
    position: m_latlng1,
    map: map
  });

  var m_latlng2 = new google.maps.LatLng(31.828463, 131.207751);
  var marker2 = new google.maps.Marker({
    position: m_latlng2,
    map: map
  });
}
