$(document).ready(function() {
	console.log('start');

	var video = document.getElementById('myVideo');
	// Opera getUserMedia
	navigator.webkitGetUserMedia("audio, video",
		function (stream) {
			console.log('success');
			console.log(stream);
			
			video.src = window.webkitURL.createObjectURL(stream);
		},
		function (err) {
			console.log('error');
			console.log(err);				
		}
	);

	console.log('end');
});
function save() {
	console.log('save');
	navigator.webkitGetUserMedia("video",
		function (stream) {
			console.log(stream);
			$('#pic img').remove();
			
			var canvas = document.createElement('canvas');
			var video = document.getElementById('myVideo');

			canvas.width = video.width;
			canvas.height = video.height;
			var ctx = canvas.getContext('2d');
			ctx.drawImage(video, 0, 0);
			var img = new Image();
			img.src = canvas.toDataURL('image/jpeg');
			$('#pic').append(img);
/*			
			if(undefined != img.src) {
 				var data = {
					picture : img.src.replace('data:image/jpeg;base64,', '')
				}
				$.ajax({
					  type: 'POST',
					  url: 'test_post/post',
					  data: data,
					  success: function(res) {
						  console.log('success');
					  }
				});
			}
*/
		},
		function (err) {
			console.log('error');
			console.log(err);				
		}
	);
}
